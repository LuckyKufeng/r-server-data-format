# FHIR Message Format

Currently, the R server can provide two cancer predications 
1. Breast cancer
2. Ovary cancer

The FHIR standard which we are using are mainly [FamilyMemberHistory](https://www.hl7.org/fhir/familymemberhistory.html), 
[Patient](https://www.hl7.org/fhir/patient.html) , [Condition](https://www.hl7.org/fhir/condition.html) and [RiskAssessment](https://www.hl7.org/fhir/riskassessment.html)
## Request data structures

### Resource Structure
Use to store cancer information of family members
```json
{
	"resourceType" : "FamilyMemberHistory",
	"id": <string>, // Required, if we expect to store request data in the database
	"status": <code>, // values: partial | completed | entered-in-error | health-unknown 
	"patient": {
		"resourceType": "Patient",
		"id": <id> // Patient ID
		"name": <string>,
		"gender": <code>, // values: male | female | other | unknown
		"birthDate": <date>
},
	"name": <string>, // the name of family member
	"date": <dateTime>, // When history was 	captured/updated
	"relationship": {CodeableConcept}, // Relationship to the subject. Examples: NFTH (natural father), NMTH (natural mother)
	"gender": <code>, // values: male | female | other | unknown
	"bornDate": <date>,
	"ageAge": Age, // the person's (approximate) age.
	"deceasedBoolean": boolean,
	"deceasedAge": Age, // Required, if the person died
	"note": Annotation, // General note about related person. Here we will put the race of this family member. Example: {"race": <string>}
	"condition" : [
		{
			"code": {CodeableConcept},
			"outcome": {CodeableConcept},
			"onsetAge": Age,
			"note": Annotation // extra information about the condition
		}
	]
}
```
If in the Json object, the value of `deceasedBoolean` is `true`, then it must to have `deceasedAge` attribute. 
If the value of `deceasedBoolean` is `false`, then `ageAge` can be optional.
	* If `ageAge` has been set, the program will take the value of `ageAge` as the person's current age.
	* If `ageAge` has **not** been set, the program will calculate the person's current age based on `bornDate`

### Request Data Example
#### FamilyMemberHistory
```json
{
	"resourceType" : "FamilyMemberHistory",
	"id": "timestamp_abc", // unique id for this record
	"status": "completed", // values: partial | completed | entered-in-error | health-unknown 
	"patient": {
		"resourceType": "Patient",
		"id": "patient_001", // Patient ID
		"name": "Peter Pan",
		"gender": "male", // values: male | female | other | unknown
		"birthDate": "1950-01-01"
	},
	"name": "Lily Pan", // the name of family member
	"date": "2018-01-01", // When history was captured/updated
	"relationship": {
        "coding":[
            {
                "system":"http://hl7.org/fhir/v3/RoleCode",
                "code":"DAU",
                "display":"natural daughter"
            }
        ]
    }, // Relationship to the subject. Examples: NFTH (natural father), NMTH (natural mother)
	"gender": "female", // values: male | female | other | unknown
	"bornDate": "1975-01-01",
	"ageAge": {
                "value": 43,
                "unit": "yr",
                "system": "http://unitsofmeasure.org",
                "code": "a"
            },
    "deceasedBoolean" : false,
	"note": {
		"text": "nonAJ"
	}, // General note about related person. Here we will put the race of this family member. Example: {"text": <string>}
	"condition" : [
		{
			"code": {
				"text":"AgeBreast"
			},
			"outcome": {
				"text":"Affected"
			}, 
			"onsetAge": {
                "value": 35,
                "unit": "yr",
                "system": "http://unitsofmeasure.org",
                "code": "a"
            },
			"note": {
				"text": "One side Breast Cancer"
			} // Optional. Extra information about the condition
		},
		{
			"code": {
				"text":"AgeBreastContralateral"
			},
			"outcome": {
				"text":"Affected"
			}, 
			"onsetAge": {
                "value": 38,
                "unit": "yr",
                "system": "http://unitsofmeasure.org",
                "code": "a"
            },
			"note": {
				"text": "Bilateral Breast Cancer"
			} // Optional. Extra information about the condition
		},
		{
			"code": {
				"text":"AgeOvary"
			},
			"outcome": {
				"text":"Affected"
			}, 
			"onsetAge": {
                "value": 38,
                "unit": "yr",
                "system": "http://unitsofmeasure.org",
                "code": "a"
            },
			"note": {
				"text": "Ovarian Cancer"
			} // Optional. Extra information about the condition
		}	
	]
}
```

## Response data structure

### RiskAssessment

```json
{
	"resourceType": "RiskAssessment",
	"id": <string>, // Required, if we expect to store response data in the database
	"status": <code>, // values: registered | preliminary | final | amended
	"method": {CodeableConcept}, // Evaluation mechanism
	"subject": Reference(Patient),
	"occurrenceDateTime": <dateTime>, // When was assessment made?
	"performer": [Reference(Practitioner|Organization|Patient|RelatedPerson)], // optional
	"prediction": // key results of risk prediction
	[
		{
			"outcome": {CodeableConcept}, // Possible outcome for the subject
			"probabilityDecimal": <decimal>,
			"whenPeriod": { 
				"low": {
					"value": <integer>, // age
					"unit": <string>, // unit is "years"
				},
				"high":{
					"value": <integer>, // age
					"unit": <string>, // unit is "years"
				}
			}
		}
	]
}

```
### Output data example 
```json
	{
		"resourceType": "RiskAssessment",
		"id": "genetic",
		"status": "final",
		"method": {
		    "coding": [
		      {
		        "code": "BRCAPRO"
		      }
		    ]
		  },
		"occurrenceDateTime": "2006-01-13T23:01:00Z",
		"prediction": [
	    {
	      "outcome": {
	        "text": "One side Breast Cancer"
	      },
	      "probabilityDecimal": 0.000168,
	      "whenRange": {
	        "high": {
	          "value": 53,
	          "unit": "years"
	        }
	      }
	    },
	    {
	      "outcome": {
	        "text": "One side Breast Cancer"
	      },
	      "probabilityDecimal": 0.000368,
	      "whenRange": {
	        "low": {
	          "value": 54,
	          "unit": "years"
	        },
	        "high": {
	          "value": 57,
	          "unit": "years"
	        }
	      }
	    },
	    {
	      "outcome": {
	        "text": "One side Breast Cancer"
	      },
	      "probabilityDecimal": 0.000594,
	      "whenRange": {
	        "low": {
	          "value": 58,
	          "unit": "years"
	        },
	        "high": {
	          "value": 62,
	          "unit": "years"
	        }
	      }
	    }]
	}
```
There are also some examples about [RiskAssessment](https://www.hl7.org/fhir/riskassessment-examples.html)