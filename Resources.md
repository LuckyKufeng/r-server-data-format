﻿## Things need to know
Goals:
Develop a wrapper app for BayesMendel R package to calculate the risk prediction of cancers. The app should to be able to convert the input data, which is in **FHIR format**, into a data matrix which matches R package input requirements. Also authorization, validation features should be implemented in the app as well. 

Server Framework: [Python Flask](http://flask.pocoo.org)
Deployment: [Docker](https://docs.docker.com) Please Install Docker on your loal machine
Authorization: [OAuth 2.0](https://oauth.net/2/)
Data Standards: [FHIR](https://www.hl7.org/fhir/overview.html), [FHIR resources list](https://www.hl7.org/fhir/resourcelist.html)


----------
### Installation guide:

1) R package: [BayesMendel](https://projects.iq.harvard.edu/bayesmendel/bayesmendel-r-package)

**Don't share this R package with anyone else!**

 1. Install R and RStudio
 2. Download R package from Google Drive
 3. Install Kinship2 `install.packages("kinship2")`
 4. Install BayesMendel, For mac, `install.packages("~/Downloads/BayesMendel_2.1-3-unix-mac.tar", repos = NULL, type = "source")`, change the path param to match your local path.


2) [Install Git](https://www.digitalocean.com/community/tutorials/how-to-install-git-on-ubuntu-14-04)
```shell
sudo apt-get update
sudo apt-get install git

```
follow this tutorial to set up git on your local machine.

3) Install python

By default, python are installed in Ubuntu
Try `python -V` or `python3 -V` to see your python version.
For this project, `python 2.7` and `python 3.3` are both OK.

4) Install PyCharm IDE 
[PyCharm](https://www.jetbrains.com/pycharm/download/#section=linux) is one of the best IDE for python development.
Instead of doing `pip` install python packages via command line, you can install/uninstall packages in IDE interface.
`Community` is the free version. 

5) Download the sample app from Git
```shell

	mkdir new_fold_of_python_project
	cd ~/new_fold_of_python_project
	git init
	git remote add origin `the url of remote git repository`
	git pull origin master
```
You can use git clone as well.

6) Test the sample app in PyCharm

