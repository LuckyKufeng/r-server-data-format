﻿
| Code | Relationship Name|
|--|--|
| ONESELF | self |
| SPS | spouse |
| SON | natural son |
| DAU | natural daughter |
| NMTH | natural mother |
| NFTH | natural father |
| NBRO | natural brother |
| NSIS | natural sister |
| MAUNT | maternal aunt |
| PAUNT | paternal aunt |
| MUNCLE | maternal uncle |
| PUNCLE | paternal uncle |
| MGRFTH | maternal grandfather |
| MGRMTH | maternal grandmother |
| PGRFTH | paternal grandfather |
| PGRMTH | paternal grandmother |
